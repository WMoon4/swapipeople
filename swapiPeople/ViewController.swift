//
//  ViewController.swift
//  swapiPeople
//
//  Created by V.K. on 3/10/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let service = PeopleService()
    var characters = [Result]()
    
    
    @IBOutlet weak var peopleTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        peopleTableView.dataSource = self
        peopleTableView.delegate = self
        peopleTableView.register(UINib(nibName: "PersonTableViewCell", bundle: nil), forCellReuseIdentifier: "personTableViewCell")
        
        service.getPeople(completion: { [weak self] characters in
            self?.characters = characters
            self?.peopleTableView.reloadData()
        })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "personTableViewCell", for: indexPath) as? PersonTableViewCell else { return UITableViewCell() }
        cell.setupCell(characters[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let profileVC: ProfileViewController = storyboard?.instantiateViewController(identifier: "Profile Controller") as? ProfileViewController {
            profileVC.characterDetails = String(describing: characters[indexPath.row])
            navigationController?.pushViewController(profileVC, animated: true)
        }
        //print(characters[indexPath.row])
        
    }
}

