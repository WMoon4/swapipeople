//
//  PersonTableViewCell.swift
//  swapiPeople
//
//  Created by V.K. on 3/10/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var hairColorLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(_ character: Result) {
        nameLabel.text = character.name
        genderLabel.text = character.gender.rawValue
        hairColorLabel.text = character.hairColor
    }
    
    
}
