//
//  ProfileViewController.swift
//  swapiPeople
//
//  Created by V.K. on 3/10/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    var characterDetails: String?
    
    @IBOutlet weak var profileLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let text = characterDetails {
            profileLabel.text = text
        }
    }
    


}
