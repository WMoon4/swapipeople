//
//  peopleService.swift
//  swapiPeople
//
//  Created by V.K. on 3/10/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import Foundation

struct PeopleService {
    private let session = URLSession.shared
    
    func getPeople(completion: @escaping (([Result]) -> Void)) {
        guard let url = URL(string: "https://swapi.co/api/people/") else { return }
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200:
                guard let data = data else { return }
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(Welcome.self, from: data)
                    print(response.results)
                    DispatchQueue.main.async {
                        completion(response.results)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print(response.statusCode, "Bad response")
            }
        }
        task.resume()
    }
}
